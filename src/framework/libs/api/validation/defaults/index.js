module.exports = [
  require("./signin"),
  require("./signup"),
  require("./file-upload"),
];