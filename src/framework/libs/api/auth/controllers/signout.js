const createSignOutController = ({
  invalidateToken,
}) => {
  const signout = ({ }) => async (req, res) => {
    await Promise.resolve(invalidateToken(req.token));

    return res.status(204).send();
  };

  return {
    path: "/signout",
    auth: true,
    private: true,
    use: {
      method: "DELETE",
      controller: signout,
    }
  }
};

module.exports = { createSignOutController };
