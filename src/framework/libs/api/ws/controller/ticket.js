const createTicketController = tickets => {
  const createTicket = ({ }) => async (req, res) => {
    try {
      const ticket = await tickets.saveTicket({
        data: req.body,
        user: req.user,
      });

      return res.status(200).json({ ticket });
    } catch (error) {
      return res.status(400).json({ error: error || "Unknown" });
    }
  };

  return {
    path: "/tickets",
    auth: true,
    private: true,
    // No need for schema - Custom logic for creating Tickets
    use: {
      method: "POST",
      controller: createTicket,
    }
  };
};

module.exports = { createTicketController };
