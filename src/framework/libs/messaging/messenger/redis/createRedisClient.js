const redis = require("redis");
const asyncRedis = require("async-redis");

const createRedisClient = ({ host, port }, async = false) => {
  const client = redis.createClient({ host, port });

  if (async) return asyncRedis.decorate(client);
  return client;
};


module.exports = { createRedisClient };