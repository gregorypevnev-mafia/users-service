const createPublisher = (client, {
  channels: {
    messageChannel,
    controlChannel
  },

  messages: {
    closeMessage,
    infoMessage
  }
}) => {
  const message = async message => {
    await client.publish(messageChannel, JSON.stringify(message));
  };

  const close = async socketId => {
    await client.publish(controlChannel, JSON.stringify({
      type: closeMessage,
      socket: socketId,
    }));
  }

  const info = async info => {
    await client.publish(controlChannel, JSON.stringify({
      type: infoMessage,
      info
    }));
  }

  return {
    message,
    close,
    info,
  };
};

module.exports = { createPublisher };
