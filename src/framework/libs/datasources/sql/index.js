const { createSQLDatasource } = require("./sqlDatasource");

module.exports = {
  name: "sql",
  creator: createSQLDatasource,
};