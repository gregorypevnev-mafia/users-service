const prefixPath = path => {
  const lastChar = path.length - 1;

  return path[lastChar] === '/' ? path.slice(0, lastChar) : path
};

const suffixPath = path => {
  return path[0] === '/' ? path.slice(1) : path
};

const combinePath = (prefix, suffix) => `${prefixPath(prefix)}/${suffixPath(suffix)}`;

const clientEndpointDecorator = (client, base) => {
  client.endpoint = path => combinePath(base, path);

  return client;
};

module.exports = { clientEndpointDecorator };
