const { createRemoteClient } = require("./remote");
const { createCircuitBreaker } = require("./circuit-breaker");
const { clientEndpointDecorator } = require("./endpoints");

const DEFAULT_TIMEOUT = 1000; // Waiting for 1s before terminating request
const DEFAULT_FAILURES = 3; // Failures to open a circuit
const DEFAULT_RETRIES = 3; // Retries for each request (Single Try)
const DEFAULT_CIRCUIT_TIMEOUT = 5000; // Circuit is open for 5s
const DEFAULT_RETRY_TIMEOUT = 200; // Timeout between retries

const circuitBreakerConfig = ({
  failureCount,
  failureTimeout,
  retriesCount,
  retriesTimeout,
} = {}) => ({
  failure: {
    failureCount: failureCount || DEFAULT_FAILURES,
    failureTimeout: failureTimeout || DEFAULT_CIRCUIT_TIMEOUT,
  },
  retries: {
    retriesCount: retriesCount || DEFAULT_RETRIES,
    retriesTimeout: retriesTimeout || DEFAULT_RETRY_TIMEOUT
  }
});

const createServiceClient = ({
  url,
  timeout,
  circuitBreaker
}) =>
  clientEndpointDecorator(
    createCircuitBreaker(
      createRemoteClient({
        url,
        timeout: timeout || DEFAULT_TIMEOUT,
      }),
      circuitBreakerConfig(circuitBreaker)
    ),
    url
  );

module.exports = { createServiceClient };

// Note: Fallbacks should be use-case specific 
//  Reads: Providing Default-Value (If NOT critical)
//  Writes: Throwing an error to terminaste the execution
