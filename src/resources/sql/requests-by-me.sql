SELECT
  requests.id AS id,
  users.id AS user_id,
  users.username AS user_username
FROM friends AS requests
  JOIN users ON requests.user2 = users.id
WHERE (
	requests.user1 = 'Current-User-ID'
  AND requests.status = 'PENDING'
);