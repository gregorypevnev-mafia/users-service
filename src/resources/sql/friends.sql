SELECT
  my_friends.id,
  my_friends.user_id,
  my_friends.username
FROM (
    -- Requests that I have sent
    SELECT
      friends.id AS id,
      users.id AS user_id,
      users.username AS username,
      friends.confirmed AS confirmed
    FROM friends
      JOIN users ON friends.user2 = users.id
    WHERE friends.user1 = 'Current-User-ID'
  UNION
    -- Requests that were sent to me
    SELECT
      friends.id AS id,
      users.id AS user_id,
      users.username AS user_username,
      friends.confirmed AS confirmed
    FROM friends
      JOIN users ON friends.user1 = users.id
    WHERE friends.user2 = 'Current-User-ID'
) AS my_friends
-- Only requests that were confirmed
WHERE my_friends.status = 'CONFIRMED';