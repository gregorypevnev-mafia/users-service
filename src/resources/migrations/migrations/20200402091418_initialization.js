const UUID_LENGTH = 36;

exports.up = function (knex) {
  const usersTablePromise = knex.schema.createTable("users", builder => {
    builder.string("id", UUID_LENGTH).primary("USERS_PK");
    builder.string("username", 255).notNullable();
    builder.string("password", 255).notNullable();
    builder.string("image", 255).notNullable();
    builder.decimal("at", 16, 0).notNullable().defaultTo(0);
    builder.unique("username", "USERS_USERNAME_UNIQUE_INDEX");
  });

  const friendsTablePromise = knex.schema.createTable("friends", builder => {
    builder.string("id", UUID_LENGTH).primary("FRIENDS_PK");
    builder.string("user1", UUID_LENGTH).notNullable();
    builder.string("user2", UUID_LENGTH).notNullable();
    builder.boolean("confirmed").defaultTo(false);
    builder.decimal("at", 16, 0).notNullable().defaultTo(0);
    builder.foreign("user1", "FRIENDS_USER_FK_1")
      .references("id")
      .inTable("users")
      .onDelete("CASCADE")
      .onUpdate("CASCADE");
    builder.unique(["user1", "user2"], "FRIENDS_UNIQUE_USERS");
  });

  return Promise.all([usersTablePromise, friendsTablePromise]);
};

exports.down = function (knex) {
  return knex.schema.dropTable("friends").then(
    () => knex.schema.dropTable("users")
  );
};
