const { microserviceBuilder } = require("./framework");

(async function () {
  const microservice = await microserviceBuilder()
    .withEvents(require("./app/events.json"))
    .addCommands(require("./app/commands"))
    .addQueries(require("./app/queries"))
    .addServices(require("./app/services"))
    .addRepositories(require("./app/repositories"))
    .withApi(require("./app/api"))
    .withSchemas(require("./app/schemas"))
    .useInfo()
    .build();

  microservice.start();
}());
