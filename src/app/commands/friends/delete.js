const { verifyFriend } = require("./utils/verifyFriend");
const { processFriend } = require("./utils/processFriend");

const type = "delete-friend";

const handler = ({
  errors,
  events,
  repositories: { friends },
  services: { }
}) => async ({ user, friendId }) => {
  const { other } = await friends.find(friendId)
    .then(verifyFriend(errors)(user))
    .then(processFriend(user));

  try {
    await friends.remove(friendId);

    await events.emit("friend-deleted", {
      id: friendId,
      by: user,
      for: other,
    });
  } catch (e) {
    throw { message: "Could not unfriend user" };
  }
};

module.exports = {
  type,
  handler
};