const friendship = (current, other) =>
  ({
    current,
    other,
  });

const processFriend = user => ({ user1, user2 }) => {
  if (user1 === user.id) return friendship(user1, user2);

  if (user2 === user.id) return friendship(user2, user1);

  throw { message: "Invalid friend data" };
}

module.exports = { processFriend };
