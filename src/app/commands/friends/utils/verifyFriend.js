const verifyFriend = errors => user => friend => {
  if (!friend)
    errors.notFound("Friend not found");

  if ([friend.user1, friend.user2].indexOf(user.id) === -1)
    errors.forbidden("Not a friend");

  if (!friend.confirmed)
    errors.error("Request still not confirmed");

  return friend;
}

module.exports = { verifyFriend };
