const type = "check-user";

const handler = ({
  repositories: { users }, // TODO: Caching
  services: { passwords, formatter }
}) => async ({ username, password }) => {
  const user = await users.findByUsername(username);
  if (user === null) return null;

  const result = await passwords.check(user.password, password);
  if (!result) return null;

  return formatter.user(user);
}

module.exports = {
  type,
  handler
};