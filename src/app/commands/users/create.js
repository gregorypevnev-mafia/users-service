const type = "create-user";

const handler = ({
  errors,
  repositories: { users }, // TODO: Caching
  services: { ids, time, passwords, formatter }
}) => async ({ username, password, image }) => {
  try {
    const securePassword = await passwords.hash(password);

    const user = {
      id: ids.generate(),
      username,
      password: securePassword,
      image,
      at: time.timestamp(),
    };

    await users.create(user);

    return formatter.user(user);
  } catch (e) {
    if (e.exists) errors.error("User with such username already exists");

    errors.error("Could not create user");
  }
};

module.exports = {
  type,
  handler
};