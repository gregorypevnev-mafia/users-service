module.exports = [
    require('./users'),
    require('./requests'),
    require('./friends')
];