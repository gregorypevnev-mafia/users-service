const type = "send-request";

const handler = ({
  errors,
  events,
  repositories: { users, friends },
  services: { ids, time }
}) => async ({
  fromUser,
  toUser: { username }
}) => {
    const toUser = await users.findByUsername(username);

    if (toUser === null) errors.notFound("User not found");

    if (fromUser.id === toUser.id) errors.error("Cannot send request to yourself");

    const check = await friends.findForUsers(fromUser.id, toUser.id);

    if (check !== null)
      errors.error(check.confirmed ? "Already friends with the user" : "Request already sent");

    try {
      const requestId = ids.generate();
      const requestedAt = time.timestamp();

      const friendRequest = {
        id: requestId,
        user1: fromUser.id,
        user2: toUser.id,
        at: requestedAt,
        confirmed: false,
      };

      await friends.create(friendRequest);

      await events.emit("request-sent", {
        id: requestId,
        from: fromUser,
        to: toUser,
        at: requestedAt
      });

      return friendRequest;
    } catch (e) {
      if (e.exists) return errors.error("Already friends with the user");

      return errors.error("Could not send friendship request");
    }
  };

module.exports = {
  type,
  handler
};