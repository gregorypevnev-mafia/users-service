const { verifyRequest } = require("./utils/verifyRequest");
const { requestChanged } = require("./utils/requestChanged");

const type = "reject-request";

const handler = ({
  errors,
  events,
  repositories: { friends },
  services: { }
}) => async ({ user, requestId }) => {
  const friendRequest = await friends.find(requestId).then(verifyRequest(errors)(user));

  try {
    await friends.remove(requestId);

    await events.emit("request-rejected", requestChanged(friendRequest, user));
  } catch (e) {
    throw errors.error("Could not reject friendship request");
  }
};

module.exports = {
  type,
  handler
};