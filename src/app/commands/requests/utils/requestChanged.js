const requestChanged = (request, user) => ({
  id: request.id,
  for: request.user1,
  by: user,
});

module.exports = { requestChanged };
