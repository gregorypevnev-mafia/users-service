const verifyRequest = errors => user => request => {
  if (request === null)
    errors.notFound("Request not found");

  if (request.user2 !== user.id)
    errors.foridden("Request is for a different user");

  if (request.confirmed)
    errors.error("Request has already been confirmed");

  return request
};

module.exports = {
  verifyRequest,
};
