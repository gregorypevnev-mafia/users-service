const { verifyRequest } = require("./utils/verifyRequest");
const { requestChanged } = require("./utils/requestChanged");

const type = "accept-request";

const handler = ({
  errors,
  events,
  repositories: { friends },
  services: { }
}) => async ({ user, requestId }) => {
  const friendRequest = await friends.find(requestId).then(verifyRequest(errors)(user));

  try {
    await friends.update(requestId, { confirmed: true });

    await events.emit("request-accepted", requestChanged(friendRequest, user));

    return friendRequest;
  } catch (e) {
    throw errors.error("Could not accept friendship request");
  }
};
module.exports = {
  type,
  handler
};