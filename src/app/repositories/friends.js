const { findOne } = require("./utils/find");

const name = "friends";
const datasource = "database";
const transactional = false;

const UNIQUE_CONSTRAINT = "FRIENDS_UNIQUE_USERS";

const find = knex => id =>
  findOne(knex("friends").select().where({ id }));

const findForUsers = knex => (fromUser, toUser) =>
  findOne(
    knex("friends").select().where({
      user1: fromUser,
      user2: toUser,
    }).orWhere({
      user1: toUser,
      user2: fromUser,
    })
  );

const create = knex => async user => {
  try {
    await knex("friends").insert(user).returning("id");
  } catch (e) {
    if (e.constraint === UNIQUE_CONSTRAINT)
      throw { exists: true, error: e };

    throw { error: e };
  }
};

const remove = knex => async id => {
  try {
    await knex("friends").delete().where({ id });
  } catch (e) {
    throw { error: e };
  }
};

const update = knex => async (id, data) => {
  try {
    await knex("friends").update(data).where({ id });
  } catch (e) {
    if (e.constraint === UNIQUE_CONSTRAINT)
      throw { exists: true, error: e };

    throw { error: e };
  }
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    find,
    findForUsers,
    create,
    remove,
    update,
  },
};