const { findOne } = require("./utils/find");

const name = "users";
const datasource = "database";
const transactional = false;

const UNIQUE_CONSTRAINT = "USERS_USERNAME_UNIQUE_INDEX";

const findByUsername = knex => username =>
  findOne(knex("users").select().where({ username }));

const create = knex => async user => {
  try {
    await knex("users").insert(user).returning("id");
  } catch (e) {
    if (e.constraint === UNIQUE_CONSTRAINT)
      throw { exists: true, error: e };

    throw { error: e };
  }
};

module.exports = {
  name,
  datasource,
  transactional,
  operations: {
    create,
    findByUsername,
  },
};