const findOne = async query => {
  try {
    const results = await query;

    if (results.length === 0) return null;
    return results[0];
  } catch (e) {
    return null;
  }
};

module.exports = { findOne };
