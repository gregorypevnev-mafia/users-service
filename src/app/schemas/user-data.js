const yup = require("yup");

const name = "user-data";

const schema = yup.object({
  username: yup.string()
    .max(50, "Username is too long")
    .min(2, "Username is too short")
    .required("Username is required"),
  password: yup.string()
    .max(50, "Password is too long")
    .min(6, "Password is too short")
    .required("Password is required"),
  image: yup.string().required("Image is required"),
});

module.exports = {
  name,
  schema,
};