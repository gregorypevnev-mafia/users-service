const yup = require("yup");

const name = "user-check";

const schema = yup.object({
  username: yup.string().required("Username is required"),
  password: yup.string().required("Password is required"),
});

module.exports = {
  name,
  schema,
};