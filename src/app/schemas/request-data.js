const yup = require("yup");

const name = "request-data";

const schema = yup.object({
  username: yup.string().required("Username is required"),
});

module.exports = {
  name,
  schema,
};