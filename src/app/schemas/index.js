module.exports = [
    require('./user-data'),
    require('./user-check'),
    require('./request-data')
];