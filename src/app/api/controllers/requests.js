const list = ({ queries }) => async (req, res) => {
  const requests = await queries.query("list-requests", req.user);

  return res.status(200).json({ requests });
};

const create = ({ commands }) => async (req, res) => {
  const { id } = await commands.call("send-request", {
    fromUser: req.user,
    toUser: req.body,
  });

  return res.status(200).json({
    request: { id }
  });
};

const remove = ({ commands }) => async (req, res) => {
  const id = String(req.params.id);

  await commands.call("reject-request", {
    user: req.user,
    requestId: id,
  });

  return res.status(204).json();
};

const update = ({
  commands,
  queries,
  services: { formatter },
}) => async (req, res) => {
  const id = String(req.params.id);

  const { id: friendId, user1 } = await commands.call("accept-request", {
    user: req.user,
    requestId: id,
  });

  const user = await queries.query("user-details", user1);

  const friend = formatter.friend({ friendId }, user);

  return res.status(200).json({ friend });
};

module.exports = {
  path: "/requests",
  middleware: [{ name: "user-info" }],
  auth: true,
  private: true,
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "GET",
        controller: list,
      }
    }, {
      path: "/",
      middleware: [],
      use: {
        method: "POST",
        controller: create,
      },
      schema: "request-data"
    }, {
      path: "/:id",
      middleware: [],
      use: {
        method: "DELETE",
        controller: remove,
      }
    }, {
      path: "/:id",
      middleware: [],
      use: {
        method: "PUT",
        controller: update,
      }
    },
  ],
};
