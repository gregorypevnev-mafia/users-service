const check = ({ commands }) => async (req, res) => {
  const user = await commands.call("check-user", req.body);

  if (user === null) return res.status(400).json({
    error: { message: "Invalid username or password" }
  })
  return res.status(200).json({ user });
};

module.exports = {
  path: "/users/check",
  use: {
    method: "POST",
    controller: check,
  },
  schema: "user-check"
};
