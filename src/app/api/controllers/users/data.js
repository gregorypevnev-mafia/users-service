const create = ({ commands }) => async (req, res) => {
  const user = await commands.call("create-user", req.body);

  return res.status(201).json({ user });
};

const find = ({ queries }) => async (req, res) => {
  const id = String(req.params.id);

  if (req.user.id !== id)
    return res.status(403).json({
      error: { message: "Cannot disclose user's information" }
    });

  const user = await queries.query("user-details", id);

  if (user === null)
    return res.status(404).json({
      error: { message: "User not found" }
    });

  return res.status(200).json({ user });
};

module.exports = {
  path: "/users",
  use: [
    {
      path: "/",
      use: {
        method: "POST",
        controller: create,
      },
      schema: "user-data"
    }, {
      path: "/:id",
      auth: true,
      private: true,
      use: {
        method: "GET",
        controller: find,
      },
    },
  ],
};
