module.exports = {
  path: "/",
  use: [
    require("./users/data"),
    require("./users/check"),
    require("./friends"),
    require("./requests")
  ],
};