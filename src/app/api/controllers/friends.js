const list = ({ queries }) => async (req, res) => {
  const friends = await queries.query("list-friends", req.user);

  return res.status(200).json({ friends });
};

const remove = ({ commands }) => async (req, res) => {
  const id = String(req.params.id);

  await commands.call("delete-friend", {
    user: req.user,
    friendId: id,
  });

  return res.status(204).send();
};

module.exports = {
  path: "/friends",
  middleware: [{ name: "user-info" }],
  auth: true,
  private: true,
  use: [
    {
      path: "/",
      middleware: [],
      use: {
        method: "GET",
        controller: list,
      }
    }, {
      path: "/:id",
      middleware: [],
      use: {
        method: "DELETE",
        controller: remove,
      }
    },
  ],
};
