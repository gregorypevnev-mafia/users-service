const name = "user-info";

// Enriching user's details from JWT to make sure their data is as complete as possible
const handler = ({ queries }) => ({ }) => async (req, res, next) => {
  if (!req.user)
    return res.status(401).json({
      error: { message: "Not authenticated" }
    });

  const userInfo = await queries.query("user-details", req.user.id);

  if (userInfo === null)
    return res.status(404).json({
      error: { message: "User credentials not found" }
    });

  req.user = userInfo;

  return next();
};

module.exports = {
  name,
  handler,
};