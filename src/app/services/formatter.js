const name = "formatter";

const user = () => ({ password, at, ...details }) => ({
  ...details,
  at: Number(at),
});

const friend = () => ({ friendId }, { id, username, image, at }) => ({
  friendId,
  user: {
    id,
    username,
    image,
    at: Number(at),
  }
})

module.exports = {
  name,
  functions: {
    user,
    friend,
  }
};