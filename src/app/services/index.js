module.exports = [
    require('./ids'),
    require('./passwords'),
    require('./formatter'),
    require('./time')
];