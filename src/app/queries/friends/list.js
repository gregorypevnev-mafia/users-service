const type = "list-friends";
const datasource = "database";

const FRIEND_DATA = {
  "friendId": "friends.id",
  "id": "users.id",
  "username": "users.username",
  "image": "users.image",
  "at": "friends.at",
};

const handler = knex => ({
  services: { formatter }
}) => async ({ id }) =>
    knex.select().from(
      knex("users")
        .select(FRIEND_DATA)
        .join(
          "friends",
          "friends.user2",
          "users.id"
        )
        .where("friends.user1", id)
        .andWhere("friends.confirmed", true)
        .union(
          knex("users")
            .select(FRIEND_DATA)
            .join(
              "friends",
              "friends.user1",
              "users.id"
            )
            .where("friends.user2", id)
            .andWhere("friends.confirmed", true)
        ).as("friend_data")
    ).orderBy("at", "desc")
      .then(friends =>
        friends.map((data =>
          formatter.friend(data, data))
        )
      );

// .orderBy("friends.at", "ASC")

module.exports = {
  type,
  datasource,
  handler,
};