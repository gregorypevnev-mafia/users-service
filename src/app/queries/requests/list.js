const type = "list-requests";
const datasource = "database";

const REQUEST_DATA = {
  "id": "requests.id",
  "fromId": "users.id",
  "fromUsername": "users.username",
  "fromImage": "users.image",
  "at": "requests.at",
};

const formatRequest = ({ id, fromId, fromUsername, fromImage, at }) => ({
  requestId: id,
  user: {
    id: fromId,
    username: fromUsername,
    image: fromImage
  },
  at: Number(at),
});

const mapRequests = requests => requests.map(formatRequest);

const handler = knex => ({ }) => async ({ id }) =>
  knex("users")
    .select(REQUEST_DATA)
    .join(
      knex("friends").select()
        .where({
          user2: id,
          confirmed: false
        }).as("requests"),
      "users.id",
      "requests.user1"
    ).orderBy("requests.at", "desc")
    .then(mapRequests);

module.exports = {
  type,
  datasource,
  handler,
};