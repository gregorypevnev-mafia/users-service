const type = "user-details";
const datasource = "database";

const USER_PROPERTIES = ["id", "username", "image", "at"];

const handler = knex => ({
  services: { formatter }
}) => async id => {
  const users = await knex("users").where({ id }).select(USER_PROPERTIES);

  if (users.length === 0) return null;
  return formatter.user(users[0]);
};

module.exports = {
  type,
  datasource,
  handler,
};