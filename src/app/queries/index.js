module.exports = [
    require('./users'),
    require('./friends'),
    require('./requests')
];