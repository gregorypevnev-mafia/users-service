const DEFAULT_DATABASE_HOST = "localhost";
const DATABASE_HOST = String(process.env.DATABASE_HOST || DEFAULT_DATABASE_HOST);

const DEFAULT_DATABASE_PORT = 5432;
const DATABASE_PORT = Number(process.env.DATABASE_PORT || DEFAULT_DATABASE_PORT);

const DEFAULT_DATABASE_USER = "user";
const DATABASE_USER = String(process.env.DATABASE_USER || DEFAULT_DATABASE_USER);

const DEFAULT_DATABASE_PASSWORD = "pass";
const DATABASE_PASSWORD = String(process.env.DATABASE_PASSWORD || DEFAULT_DATABASE_PASSWORD);

module.exports = {
  type: "sql",
  config: {
    database: "users",
    host: DATABASE_HOST,
    port: DATABASE_PORT,
    user: DATABASE_USER,
    password: DATABASE_PASSWORD
  }
};