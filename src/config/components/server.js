const DEFAULT_PORT = 3000;
const PORT = Number(process.env.PORT || DEFAULT_PORT);

module.exports = {
  port: PORT,
  host: "0.0.0.0"
};