#!/bin/bash

cd resources/migrations

knex migrate:latest

cd ../..

pm2 start --no-daemon ecosystem.config.js